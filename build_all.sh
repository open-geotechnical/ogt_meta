#!/bin/bash

# all stuff is output to "public/" directory
# this cannot be changed as it used by gitlab CI
# and makes it appear at open-geotechnical.gitlab.io/ogt_meta/ website

rm -f -r ./public/
mkdir public/

# Sphinx reads rst sources and generated html pages
sphinx-build -a  -b html ./docs/ ./public/


echo "Done :-)"