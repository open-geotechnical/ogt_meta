# -*- coding: utf-8 -*-

import os
from fabric.api import env, local, run, cd, lcd, sudo, warn_only, prompt, shell_env

os.environ["__GEN_DOCS__"] = "1"


ROOT_PATH =  os.path.abspath( os.path.dirname( __file__ ))

DOCS_DIR = os.path.join(ROOT_PATH, "docs")
DOCS_BUILD_DIR = os.path.join(ROOT_PATH, "public")


def svg2icons():
    """Converts the svg icons to png"""
    src_d = os.path.join(ROOT_PATH,  "svg")
    target_d = os.path.join(DOCS_DIR, "_static", "icons")

    local("mkdir -p %s" % target_d)

    output = local('ls %s' % os.path.join(src_d, "*.svg"), capture=True )
    files = output.split()
    print files

    for fp in files:
        f = os.path.basename(fp)
        print fp, f

        s_file = os.path.join(src_d, f)
        t_file = os.path.join(target_d, f[0:-4] + ".png" )
        #local("convert -resize 32x32 %s %s " % (s_file, t_file))
        local("inkscape -z -e %s -w 32 -h 32 %s" % (t_file, s_file))

        i_file = os.path.join(target_d, f[0:-4] + ".ico")
        local("convert -density 384 %s -define icon:auto-resize %s" % (s_file, i_file ))


def copy_stuff():

    dirs = ['ogt_meta', 'ogt-go', 'ogt-mobile', 'ogt-py', "open-geotechnical.gitlab.io", "unofficial-ags4"]
    for d in dirs:

        target_dir = os.path.join("home","ogt", d, "docs")
        print target_dir
        if d != "ogt_meta":


            for cp in ["_templates/navbar.html",
                       "_templates/globaltoc.html",
                       "_templates/footer.html"   ]:
                local("cp docs/%s /%s/%s" % (cp, target_dir, cp))

        i_path = os.path.join(target_dir, "_static", "favicon.ico")
        print("i_path=", i_path)

        si = d
        if d in [ "open-geotechnical.gitlab.io", "ogt_meta"]:
            si = "logo"
        local("cp docs/_static/icons/%s.ico /%s" % (si, i_path))

def lncss():
    target = os.path.join(DOCS_BUILD_DIR, "_static", "css", "ogt_sphinx.css")
    print(target)
    if os.path.exists(target):
        pass
    local("rm " + target)
    src = os.path.join(DOCS_DIR, "_static", "css", "ogt_sphinx.css" )
    with lcd(os.path.join(DOCS_BUILD_DIR, "_static", "css")):
        local("ln -s %s ." % src)
