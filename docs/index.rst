ogt_meta
====================================

ogt_meta contains the root stuff for ogt project.. mainly for style and icons..

Projects index
------------------

```
https://open-geotechnical.gitlab.io/ogt_meta/_static/ogt_projects.json
```

Style sheet
------------------

```
https://open-geotechnical.gitlab.io/ogt_meta/_static/css/ogt_sphinx.json
```


.. toctree::
   :maxdepth: 2
   :caption: Contents:



Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
