# Configuration file for the Sphinx documentation builder.
#
# This file only contains a selection of the most common options. For a full
# list see the documentation:
# http://www.sphinx-doc.org/en/master/config

# -- Path setup --------------------------------------------------------------

# If extensions (or modules to document with autodoc) are in another directory,
# add these directories to sys.path here. If the directory is relative to the
# documentation root, use os.path.abspath to make it absolute, like shown here.
#
import os
import sys
import json

# sys.path.insert(0, os.path.abspath('.'))
HERE_PATH =  os.path.abspath( os.path.dirname( __file__ ))
#sys.path.insert(0, HERE_PATH)


project = 'ogt_meta'
copyright = '2019, ogt@daffodil.uk.com'
author = 'ogt@daffodil.uk.com'




# The full version, including alpha/beta/rc tags
version = ""
release = ''


# -- General configuration ---------------------------------------------------

# Add any Sphinx extension module names here, as strings. They can be
# extensions coming with Sphinx (named 'sphinx.ext.*') or your custom
# ones.
extensions = [
]

# Add any paths that contain templates here, relative to this directory.
templates_path = ['_templates']

# List of patterns, relative to source directory, that match files and
# directories to ignore when looking for source files.
# This pattern also affects html_static_path and html_extra_path.
exclude_patterns = ['_build', 'Thumbs.db', '.DS_Store']


# -- Options for HTML output -------------------------------------------------

# The theme to use for HTML and HTML Help pages.  See the documentation for
# a list of builtin themes.
#

html_theme = 'sphinxbootstrap4theme'
import sphinxbootstrap4theme
html_theme_path = [sphinxbootstrap4theme.get_path()]
#
# -- Project information -----------------------------------------------------
def get_projects():
    with open(os.path.join(HERE_PATH, "_static", "ogt_projects.json")) as f:
        return json.load(f)

html_context = dict(projects=get_projects(), logo="logo.png")
print(html_context)

#html_logo  = "_static/icons/logo.png"
#html_logo  = None

# The name of an image file (relative to this directory) to use as a favicon of
# the docs.  This file should be a Windows icon file (.ico) being 16x16 or 32x32
# pixels large.
html_favicon = "_static/favicon.ico"
html_title = "ogt_meta"
html_sidebars = { '**': ['globaltoc.html'], }
# A shorter title for the navigation bar.  Default is the same as html_title.
html_short_title = "ogt_meta"

html_theme_options = {
    'navbar_links' : [
        ('Project Home', '/', True),
        ("ogt-py", ".ogt-py", True),
        ("Unofficial AGS4", "/unofficial-ags4", True)
    ],
    'sidebar_right': False,
    'sidebar_fixed': False,
    'navbar_style': 'fixed-top',
    #"main_width": "1200px",
    # "sidebars": "globaltoc.html",
    "navbar_bg_class": "ogt-meta"
}

# Add any paths that contain custom static files (such as style sheets) here,
# relative to this directory. They are copied after the builtin static files,
# so a file named "default.css" will overwrite the builtin "default.css".
html_static_path = [
    '_static',
]
html_show_sourcelink = True

# If true, "Created using Sphinx" is shown in the HTML footer. Default is True.
html_show_sphinx = False

# If true, "(C) Copyright ..." is shown in the HTML footer. Default is True.
#
html_show_copyright = False

